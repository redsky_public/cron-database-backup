# Cron Database Backup

The cron-database-backup module allows you to automate datbase backups on regular intervals. Currently this only supports 
mysql backups. Mysql dump files will be placed in a directory of your choice. You can choose to receive daily confirmation 
emails as well as error report emails. Error emails happen anytime something goes wrong (cannot connect to database etc.). 
Backups will never fill the entire disk space. The oldest ones will be removed first.

On first initialization, if data is missing from the required options struct an error will be thrown.

## Getting Started

Install cron-database-backup using npm:

```console
$ yarn add cron-database-backup
or 
$ npm install --save cron-database-backup
```

You will need a gmail account to send emails from. 

I wouldn't use your personal account.

After creating an account Go to : https://www.google.com/settings/security/lesssecureapps

Set the Access for less secure apps setting to Enabled (also why you shouldn't use a personal account)

## Usage

Import cron-database-backup and backup using whatever schedule you like using the options. See below for all examples.

```javascript
const cronDatabaseBackup = require('cron-database-backup');

let options = {
    backups: [{
        name: "my_database_name",
        databaseType: "mysql",
        connection: {
            host: 'database.ip.net',
            user: 'root',
            password: 'yourpassword',
            database: 'databaseName'
        },
        schedules: [{
            cronSchedule: "55 * * * *", // Hourly at XX:55.
            directory: './databaseName/hourly',
            maxBackups: 24
        }, {
            cronSchedule: "30 0 * * *",  // Daily at 12:30 a.m.
            directory: './databaseName/daily',
            maxBackups: 7
        }, {
            cronSchedule: "30 1 * * 0",  // Weekly Sunday at 1:30 a.m.
            directory: './databaseName/weekly',
            maxBackups: 4
        }, {
            cronSchedule: "0 3 1 * *",  // First Day of every month at 3:00 a.m.
            directory: './databaseName/monthly',
            maxBackups: 12
        }
        ]
    }],
    // Optional parameters follow
    reports: [{
        type: "email",
        sendTo: 'steve@apple.com',
        sendFrom: 'backup@gmail.com',
        sendFromPassword: 'gmailPassword',
        sendDailyReport: true,
        sendError: true
        }, {
        type: "slack",
        slackApiKey: 'asdfasdf-asf-asf',
        channel: '#channel',
        sendDailyReport: true,
        senError: true
    }],
    onSuccess: function({ name, filename, duration }) { console.log(`Backup Complete (${name} - file ${filename}) - Duration: ${(duration / 1000).toFixed(3)} seconds.`)},
	onError: function({ name, error }) { console.log(`Failed to backed database: ${name} with ${error}.`)}
};

cronDatabaseBackup(options);
```

## PM2

Use a process manager to restart your process if it ever fails
https://www.npmjs.com/package/pm2

## Cron Syntax

This is a quick reference to cron syntax and also shows the options supported by cron-mysql-backup.

### Allowed fields

```
 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *
```

### Allowed values

|     field    |        value        |
|--------------|---------------------|
|    second    |         0-59        |
|    minute    |         0-59        |
|     hour     |         0-23        |
| day of month |         1-31        |
|     month    |     1-12 (or names) |
|  day of week |     0-7 (or names, 0 or 7 are sunday)  |


### More Examples At
https://www.npmjs.com/package/node-cron
