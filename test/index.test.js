const should = require('chai').should();
const expect = require('chai').expect;

describe('Cron Database Tests', function() {
	describe('Init Tests', function() {
		it('Should throw if no options are provided', function() {
			let cronDbBackup = require('../index');
			expect(() => cronDbBackup()).to.throw();
		});

		it('Should throw if backup array is missing options are provided', function() {
			let cronDbBackup = require('../index');
			expect(() => cronDbBackup({})).to.throw();
		});

		it('Should throw if backup field is not an array', function() {
			let cronDbBackup = require('../index');
			expect(() => cronDbBackup({ backups: {} })).to.throw();
		});

		it('Should throw if missing database name', function() {
			let cronDbBackup = require('../index');
			let options = {
				backups: [
					{
						//name: "ontrac_sandbox",
						databaseType: 'mysql',
						connection: {
							host: 'database.ip.net',
							user: 'root',
							password: 'yourpassword',
							database: 'databaseName'
						},
						schedules: [
							{
								cronSchedule: '55 * * * *', // Hourly at XX:55.
								directory: './databaseName/hourly',
								maxBackups: 24
							}
						]
					}
				]
			};
			expect(() => cronDbBackup(options)).to.throw();
		});

		it('Should throw if missing a connection field', function() {
			let cronDbBackup = require('../index');
			let options = {
				backups: [
					{
						name: 'ontrac_sandbox',
						databaseType: 'mysql',
						connection: {
							host: 'database.ip.net',
							user: 'root',
							//password: 'yourpassword',
							database: 'databaseName'
						},
						schedules: [
							{
								cronSchedule: '55 * * * *', // Hourly at XX:55.
								directory: './databaseName/hourly',
								maxBackups: 24
							}
						]
					}
				]
			};
			expect(() => cronDbBackup(options)).to.throw();
		});

		it('Should throw if missing a schedule field', function() {
			let cronDbBackup = require('../index');
			let options = {
				backups: [
					{
						name: 'ontrac_sandbox',
						databaseType: 'mysql',
						connection: {
							host: 'database.ip.net',
							user: 'root',
							password: 'yourpassword',
							database: 'databaseName'
						},
						schedules: [
							{
								cronSchedule: '55 * * * *', // Hourly at XX:55.
								//directory: './databaseName/hourly',
								maxBackups: 24
							}
						]
					}
				]
			};
			expect(() => cronDbBackup(options)).to.throw();
		});

        it('Should throw if missing a schedule field in second array element', function() {
            let cronDbBackup = require('../index');
            let options = {
                backups: [
                    {
                        name: 'ontrac_sandbox',
                        databaseType: 'mysql',
                        connection: {
                            host: 'database.ip.net',
                            user: 'root',
                            password: 'yourpassword',
                            database: 'databaseName'
                        },
                        schedules: [
                            {
                                cronSchedule: '55 * * * *', // Hourly at XX:55.
                                directory: './databaseName/hourly',
                                maxBackups: 24
                            }, {
                                cronSchedule: "30 1 * * 0",  // Weekly Sunday at 1:30 a.m.
                                directory: './databaseName/weekly',
                                //maxBackups: 4
                            }
                        ]
                    }
                ]
            };
            expect(() => cronDbBackup(options)).to.throw();
        });

		it('Should throw if schedules is empty', function() {
			let cronDbBackup = require('../index');
			let options = {
				backups: [
					{
						name: 'ontrac_sandbox',
						databaseType: 'mysql',
						connection: {
							host: 'database.ip.net',
							user: 'root',
							password: 'yourpassword',
							database: 'databaseName'
						},
						schedules: []
					}
				]
			};
			expect(() => cronDbBackup(options)).to.throw();
		});
	});
});
