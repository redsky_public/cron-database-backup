const mysqldump = require('mysqldump');
const cron = require('node-cron');
const nodemailer = require('nodemailer');
const path = require('path');
const fs = require('fs');
const diskusage = require('diskusage');
const Slack = require('slack-node');

let fsPromises = fs.promises;

function setUpCronJobs(options) {
	validateOptions(options);

	// For sanity do a single attempt on the first schedule
	attemptBackup(options, 0, 0).catch(console.error);

	for (let i in options.backups) {
		for (let j in options.backups[i].schedules) {
			cron.schedule(options.backups[i].schedules[j].cronSchedule, () => {
				attemptBackup(options, i, j).catch(console.error);
			});
		}
	}

	cron.schedule('0 0 * * *', () => {
		for (let i in options.reports) {
			if (options.reports[i].sendDailyReport) {
				if (options.reports[i].type === 'email') sendDailyReportEmail(options, i).catch(console.error);
				else if (options.reports[i].type === 'slack') sendDailyReportSlack(options, i).catch(console.error);
			}
		}
		clearSuccessHistory(options);
	});
}

async function attemptBackup(options, backupIndex, scheduleIndex) {
	let backup = options.backups[backupIndex];
	let schedule = backup.schedules[scheduleIndex];
	try {
		await validateFreeDiskSpace(schedule.directory);
		let backupStartTime = Date.now();
		if (options.onStart) options.onStart({ name: backup.name });
		let backupFilename = await backupDatabase(backup.connection, schedule.directory);
		let backupDuration = Date.now() - backupStartTime;
		await removeOldestBackups(schedule.maxBackups, schedule.directory);

		if (options.onSuccess)
			options.onSuccess({
				name: backup.name,
				filename: backupFilename,
				duration: backupDuration
			});
		schedule.successHistory.push({ backupName: backup.name, backupFilename, backupDuration, time: new Date() });
	} catch (e) {
		if (options.onError)
			options.onError({
				name: backup.name,
				error: e
			});
		return sendFailureReport(backup, e, options.reports);
	}
}

function clearSuccessHistory(options) {
	for (let i in options.backups) {
		for (let j in options.backups[i].schedules) {
			options.backups[i].schedules[j].successHistory = [];
		}
	}
}

async function backupDatabase(connection, directory) {
	ensureDirectoryExists(directory);
	let fileName = `${new Date().toISOString()}.sql.gz`;
	if (process.platform === 'win32') {
		fileName = fileName.replace(/[\/\\:]/g, '_');
	}
	// Throws exception on failure
	await mysqldump({
		connection,
		dumpToFile: path.join(directory, fileName),
		compressFile: true
	});

	return fileName;
}

async function validateFreeDiskSpace(path) {
	await ensureDirectoryExists(path);
	// keep 10% free
	const info = await diskusage.check(path);
	if (info.free / info.total < 0.1) throw new Error('Out of disk space.');
}

async function removeOldestBackups(maxBackups, directory) {
	await ensureDirectoryExists(directory);
	let files = await fsPromises.readdir(directory);
	files = files.sort(function(a, b) {
		return (
			fs.statSync(path.join(directory, b)).mtime.getTime() - fs.statSync(path.join(directory, a)).mtime.getTime()
		);
	});
	let count = 0;
	for (let i in files) {
		if (++count > maxBackups) {
			await fsPromises.unlink(path.join(directory, files[i]));
		}
	}
}

async function sendDailyReportSlack(options, reportIndex) {
	let slack = new Slack(options.reports[reportIndex].slackApiKey);
	let averageBackupTime = 0;
	let backupList = [];

	for (let i in options.backups) {
		for (let j in options.backups[i].schedules) {
			backupList = backupList.concat(options.backups[i].schedules[j].successHistory);
		}
	}

	let message = '*Daily Backup Report*\n';
	for (let i in backupList) {
		averageBackupTime += backupList[i].backupDuration;
		message += `*Backup* - ${backupList[i].backupName} - *Date* - ${backupList[i].time} - *Duration* - ${backupList[
			i
		].backupDuration / 1000}\n`;
	}

	message += `*Average Backup Time* - ${(averageBackupTime / (backupList.length * 1000)).toFixed(3)} seconds `;
	message += `*Total Backup Count* - ${backupList.length}\n`;
	slack.api(
		'chat.postMessage',
		{
			text: message,
			channel: options.reports[reportIndex].channel
		},
		function(err, response) {
			if (err) console.error(err);
		}
	);
}

async function sendDailyReportEmail(options, reportIndex) {
	let averageBackupTime = 0;
	let backupList = [];

	for (let i in options.backups) {
		for (let j in options.backups[i].schedules) {
			backupList = backupList.concat(options.backups[i].schedules[j].successHistory);
		}
	}

	let backupMessageHtml = `<!DOCTYPE html><html><body><style>body { color: #444; }</style><h2>The following is a report of your backup history</h2>`;
	let backupListHtml = `<table style="border-collapse:collapse; border: 1px solid #a3a3a3;"><tr style="border: 1px solid #a3a3a3;">
                            <th style="border: 1px solid #a3a3a3; padding: 5px;">Date of Backup</th>
                            <th style="border: 1px solid #a3a3a3; padding: 5px;">Duration</th>
                            <th style="border: 1px solid #a3a3a3; padding: 5px;">Backup Name</th>
                            <th style="border: 1px solid #a3a3a3; padding: 5px;">Backup Filename</th></tr>`;
	for (let i in backupList) {
		averageBackupTime += backupList[i].backupDuration;
		backupListHtml += `<tr style="border: 1px solid #a3a3a3; padding: 5px;">
                            <td style="border: 1px solid #a3a3a3; padding: 5px;">${backupList[i].time}</td>
                            <td style="border: 1px solid #a3a3a3; padding: 5px;">${(
								backupList[i].backupDuration / 1000
							).toFixed(3)} seconds</td>
                            <td style="border: 1px solid #a3a3a3; padding: 5px;">${backupList[i].backupName}</td>
                            <td style="border: 1px solid #a3a3a3; padding: 5px;">${
								backupList[i].backupFilename
							}</td></tr>`;
	}
	backupListHtml += `</table>`;
	backupMessageHtml += `<p>Average Backup Time: ${(averageBackupTime / (backupList.length * 1000)).toFixed(
		3
	)} seconds</p><p>Total Backup Count: ${backupList.length}</p>`;
	backupMessageHtml += backupListHtml;
	backupMessageHtml += `</body></html>`;
	sendEmail(
		options.reports[reportIndex].sendTo,
		options.reports[reportIndex].sendFrom,
		options.reports[reportIndex].sendFromPassword,
		`Daily Backup Report ${new Date().toDateString()}`,
		backupMessageHtml
	);
}

function sendFailureReport(backup, e, reports) {
	for (let i in reports) {
		if (!reports[i].sendError) continue;
		if (reports[i].type === 'email') {
			sendFailureEmail(reports[i].sendTo, reports[i].sendFrom, reports[i].sendFromPassword, e, backup.name);
		} else if (reports[i].type === 'slack') {
			sendFailureSlack(reports[i].slackApiKey, reports[i].channel, e, backup.name);
		}
	}
}

function sendFailureSlack(slackApiKey, channel, e, name) {
	let slack = new Slack(slackApiKey);

	let message = `*Backup Failure on ${name}*\n`;
	message += `*Error* - ${JSON.stringify(e)}\n`;
	slack.api(
		'chat.postMessage',
		{
			text: message,
			channel: channel
		},
		function(err, response) {
			if (err) console.error(err);
		}
	);
}

function sendFailureEmail(sendTo, sendFrom, sendFromPassword, e, name) {
	sendEmail(
		sendTo,
		sendFrom,
		sendFromPassword,
		name + ' Backup Failed',
		'Here is the error message <br> ' + e + JSON.stringify(e)
	);
}

function sendEmail(sendTo, sendFrom, sendFromPassword, subject, body) {
	let mailConfig = {
		service: 'gmail',
		auth: {
			user: sendFrom,
			pass: sendFromPassword
		}
	};

	let transporter = nodemailer.createTransport(mailConfig);
	let mailOptions = {
		from: '"cron-mysql-backup" <' + sendFrom + '>', // sender address
		to: sendTo, // list of receivers
		subject, // Subject line
		html: body
	};
	transporter.sendMail(mailOptions).catch(console.error);
}

function validateOptions(options) {
	if (!options || !options.backups || !Array.isArray(options.backups) || options.backups.length === 0)
		throw new Error('You did not specify any backups');

	const requiredBackupFields = ['name', 'databaseType', 'connection', 'schedules'];
	const requiredConnectionFields = ['host', 'user', 'password', 'database'];
	const requiredScheduleFields = ['cronSchedule', 'directory', 'maxBackups'];

	let missingBackupFields = [];
	let missingConnectionFields = [];
	let missingScheduleFields = [];
	for (let i in options.backups) {
		missingBackupFields = missingBackupFields.concat(validateObj(options.backups[i], requiredBackupFields));
		if (options.backups[i].connection)
			missingConnectionFields = missingConnectionFields.concat(
				validateObj(options.backups[i].connection, requiredConnectionFields)
			);

		if (
			!options.backups[i].schedules ||
			!Array.isArray(options.backups[i].schedules) ||
			options.backups[i].schedules.length === 0
		)
			throw new Error('You did not specify any schedules');

		for (let j in options.backups[i].schedules) {
			// Add a history object to each schedule
			options.backups[i].schedules[j].successHistory = [];
			missingScheduleFields = missingScheduleFields.concat(
				validateObj(options.backups[i].schedules[j], requiredScheduleFields)
			);
		}
	}

	let error = '';
	if (missingBackupFields.length > 0) {
		error = 'You need to include ' + missingBackupFields.join(', ') + ' in the options.backups object.\n';
	}
	if (missingConnectionFields.length > 0) {
		error +=
			'You need to include ' +
			missingConnectionFields.join(', ') +
			' in the options.backups.connection object.\n';
	}
	if (missingScheduleFields.length > 0) {
		error +=
			'You need to include ' + missingScheduleFields.join(', ') + ' in the options.backups.schedules object.\n';
	}

	if (error.length) throw new Error(error);
}

function validateObj(object, required) {
	let missing = [];
	for (let i in required) {
		let opt = required[i];
		if (object[opt]) continue;
		missing.push(opt);
	}
	return missing;
}

function ensureDirectoryExists(dir) {
	if (fs.existsSync(dir)) return true;
	const dirname = path.dirname(dir);
	ensureDirectoryExists(dirname);
	fs.mkdirSync(dir);
}

module.exports = setUpCronJobs;
